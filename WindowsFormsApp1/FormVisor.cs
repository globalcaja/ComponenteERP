﻿using CefSharp;
using CefSharp.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    
    public partial class FormVisor : Form
    {
        private String parcelas;

        public string Parcelas { get => parcelas; set => parcelas = value; }

        public FormVisor()
        {
            InitializeComponent();
        }

        private void loadVisor(object sender, EventArgs e)
        {
            
            // Create a new browser window
           var _browser =
                new ChromiumWebBrowser("http://globalcampo.es/");
            // Add the new browser window to the form

            var angular4erp = new Angular4Erp();
            _browser.RegisterJsObject("visorsgipacerp", angular4erp );
            
            RegisterWebsite.Load(_browser);
            _browser.Load("https://globalcampo.es/");
            
            Controls.Add(_browser);
            _browser.Refresh();
            angular4erp.loadParcelas(parcelas);
        }

        private void cerrarVisor(object sender, FormClosingEventArgs e)
        {
            CefSharp.Cef.Shutdown();
        }
    }

    public class Angular4Erp
    {
        private String parcelas;
        private IJavascriptCallback callback;

        public void registerCallback(IJavascriptCallback callback)
        {

            this.callback = callback;
            if (this.parcelas != null)
            {
                this.callback.ExecuteAsync(this.parcelas);
            }

        }

        public void loadParcelas(String parcelas)
        {
            this.parcelas = parcelas;
        }
    }
}
