﻿using CefSharp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Principal : Form
    {
        public Principal()
        {
            var settings = new CefSettings();
            settings.BrowserSubprocessPath = @"x86\CefSharp.BrowserSubprocess.exe";
            settings.RemoteDebuggingPort = 8088;
            CefSharpSettings.LegacyJavascriptBindingEnabled = true;
            
            Cef.Initialize(settings, performDependencyCheck: false, browserProcessHandler: null);
            InitializeComponent();
        }

        private void verMapaAction(object sender, EventArgs e)
        {
            var visor = new FormVisor();
            visor.Parcelas = this.textBox1.Text;
            visor.Show();

            

            
        }
    }
}
