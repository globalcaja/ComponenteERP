﻿using CefSharp;
using CefSharp.WinForms;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
    class RegisterWebsite
    {
        public static void Load(ChromiumWebBrowser browser)
        {
            var factory = (DefaultResourceHandlerFactory)
                (browser.ResourceHandlerFactory);
            if (factory == null) return;
            var response = ResourceHandler
                .FromStream(LoadResource("index.html"));
            factory.RegisterHandler("https://globalcampo.es/", response);
            var resourceNames = Assembly.GetExecutingAssembly()
                .GetManifestResourceNames();
            foreach (var resource in resourceNames)
            {
                if (!resource.StartsWith("WindowsFormsApp1.web"))
                    continue;
                var url = resource.Replace("WindowsFormsApp1.web.", "");
                var r = LoadResource(url);
              //  url = url.Replace(".", "/");
              if( url.StartsWith("assets.jsontests"))
                {
                    url = url.Replace("assets.jsontests.", "assets/jsontests/");
                }
              //var lastSlash = url.LastIndexOf("/",StringComparison.Ordinal);
              //  url = url.Substring(0, lastSlash) + "." + url.Substring(lastSlash + 1);

                factory.RegisterHandler("https://globalcampo.es/" + url,
                     ResourceHandler.FromStream(r));
            }
        }
        static Stream LoadResource(string filename)
        {
            var assembly = Assembly.GetExecutingAssembly();
            var textStream = assembly
                .GetManifestResourceStream("WindowsFormsApp1.web."
                    + filename);
            return textStream;
        }
    }

}
